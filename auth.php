<?php
	session_start();

	require_once("./vendor/autoload.php");
	require_once("./proc/errors.php");

	$loader = new Twig_Loader_Filesystem('./templates');
	$twig = new Twig_Environment($loader, array(
		'cache' => './cache',
		'auto_reload' => TRUE
	));

	$login = $twig->loadTemplate('views/auth/login.html');
	$register = $twig->loadTemplate('views/auth/register.html');
	
	if(isset($_SESSION['auth'])) {
		header('Location: /');
	} else {
		if($_GET['action'] == 'login') {
			if(isset($_GET['fb'])) {
				$login->display(array('errorid' => $_GET['fb'], 'error' => getError($_GET['fb'])));
			} else {
				$login->display(array());
			}
		} elseif($_GET['action'] == 'register') {
			if(isset($_GET['fb'])) {
				$register->display(array('errorid' => $_GET['fb'], 'error' => getError($_GET['fb'])));
			} else {
				$register->display(array());
			}
		} else {
			header('Location: /');
		}
	}
?>
