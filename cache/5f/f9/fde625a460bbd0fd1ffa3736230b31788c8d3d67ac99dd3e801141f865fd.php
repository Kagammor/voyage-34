<?php

/* views/login.html */
class __TwigTemplate_5ff9fde625a460bbd0fd1ffa3736230b31788c8d3d67ac99dd3e801141f865fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Voyage 34: Register";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<section class=\"auth_register\">
\t<h1>Login</h1>
\t
\t";
        // line 9
        if (array_key_exists("error", $context)) {
            // line 10
            echo "\t\t<span class=\"error\">";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "</span>
\t";
        }
        // line 12
        echo "\t
\t";
        // line 13
        if (((!(isset($context["error"]) ? $context["error"] : null)) == 210)) {
            // line 14
            echo "\t<form method=\"post\" action=\"./proc/auth.php\">
\t\t<ul>
\t\t\t<li><input type=\"text\" maxlength=\"20\" placeholder=\"username\" name=\"username\"></li>
\t\t\t<li><input type=\"password\" placeholder=\"password\" name=\"password\"></li>
\t\t
\t\t\t<li><input type=\"submit\" value=\"Login\" name=\"login\"></li>
\t\t</ul>
\t</form>
\t";
        }
        // line 23
        echo "</form>
";
    }

    public function getTemplateName()
    {
        return "views/login.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 23,  56 => 14,  54 => 13,  51 => 12,  45 => 10,  43 => 9,  38 => 6,  35 => 5,  29 => 3,);
    }
}
