<?php

/* base.html */
class __TwigTemplate_0174e77033fc06d77fb54d170eddee574f4c553a9c42732b663ee1db5af230dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
</html>
\t<head>
\t\t<title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t\t
\t\t<link href=\"assets/css/reset.css\" type=\"text/css\" rel=\"stylesheet\">
\t\t<link href=\"assets/css/style.css\" type=\"text/css\" rel=\"stylesheet\">
\t\t
\t\t<script src=\"assets/js/jquery.js\"></script>
\t</head>
\t
\t<body>
\t\t<header>
\t\t\t<div class=\"center_wrap\">
\t\t\t\t<h1 class=\"logo\"><a href=\".\">Voyage 34</a></h1>
\t\t\t\t
\t\t\t\t<section class=\"auth_panel\">
\t\t\t\t";
        // line 18
        if (array_key_exists("username", $context)) {
            // line 19
            echo "\t\t\t\t\t<span class=\"auth_panel_welcome\">Welcome, ";
            echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : null), "html", null, true);
            echo "</span>
\t\t\t\t\t<a href=\"./profile\"><span class=\"auth_panel_link auth_panel_profile\">profile</span></a>
\t\t\t\t\t<a href=\"./proc/auth?logout\"><span class=\"auth_panel_link auth_panel_logout\">sign off</span></a>
\t\t\t\t";
        } else {
            // line 23
            echo "\t\t\t\t\t<form method=\"post\" action=\"./proc/auth.php\">
\t\t\t\t\t\t<input type=\"text\" maxlength=\"20\" placeholder=\"username\" name=\"username\">
\t\t\t\t\t\t<input type=\"password\" placeholder=\"password\" name=\"password\">
\t\t\t\t\t\t
\t\t\t\t\t\t<input type=\"submit\" value=\"Login\" name=\"login\">
\t\t\t\t\t</form>
\t\t\t\t\t
\t\t\t\t\t<span class=\"auth_panel_signup\"><a href=\"./auth?action=register\">Oh, we haven't met yet? Sign up!</a></span>
\t\t\t\t";
        }
        // line 32
        echo "\t\t\t\t</section>
\t\t\t</div>
\t\t</header>
\t\t
\t\t<section class=\"content\">
\t\t\t<div class=\"center_wrap\">
\t\t\t";
        // line 38
        $this->displayBlock('content', $context, $blocks);
        // line 39
        echo "\t\t\t</div>
\t\t</section>
\t</body>
</html>
";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
    }

    // line 38
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 38,  82 => 4,  74 => 39,  72 => 38,  64 => 32,  53 => 23,  45 => 19,  43 => 18,  26 => 4,  21 => 1,);
    }
}
