<?php

/* views/front.html */
class __TwigTemplate_be12b56269b581eaa94795d64fb59d2b281c8fb6e9d5ba18041eb92ee13ba2ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Voyage 34: Welcome";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<h1>Welcome!</h1>

<p>
Voyage 34 is a versatile platform for recording and sharing experiences with psychoactives.
</p>

<a href=\"/exp?action=add\"><button type=\"button\" class=\"button_addxp\">Add experience</button></a>
";
    }

    public function getTemplateName()
    {
        return "views/front.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,);
    }
}
