<?php

/* views/exp/add.html */
class __TwigTemplate_5cd6948fee29338383b597b96e8e27a7bb9f44d7b0d2ea301cffbeaf6fca908c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Voyage 34: Add experience";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<section class=\"auth_register\">
\t<h1>Add experience</h1>
\t
\t";
        // line 9
        if (array_key_exists("error", $context)) {
            // line 10
            echo "\t\t<span class=\"error\">";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "</span>
\t";
        }
        // line 12
        echo "\t
\t";
        // line 13
        if (array_key_exists("success", $context)) {
            // line 14
            echo "\t\t<span class=\"success\">Registration complete!</span>
\t";
        }
        // line 16
        echo "\t
\t<form method=\"post\" action=\"./proc/exp.php\">
\t\t<ul class=\"add_entry\">
\t\t\t<li>
\t\t\t\t<h2>Title</h2>
\t\t\t\t<input type=\"text\" name=\"title\" placeholder=\"Name your experience!\">
\t\t\t</li>
\t\t
\t\t\t<li>
\t\t\t\t<h2>Date</h2>
\t\t\t\t
\t\t\t\t<select name=\"date_day\">
\t\t\t\t";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 31));
        foreach ($context['_seq'] as $context["_key"] => $context["day"]) {
            // line 29
            echo "\t\t\t\t\t";
            if (((isset($context["day"]) ? $context["day"] : null) < 10)) {
                // line 30
                echo "\t\t\t\t\t\t";
                if (((isset($context["day"]) ? $context["day"] : null) == twig_date_format_filter($this->env, "now", "d"))) {
                    // line 31
                    echo "\t\t\t\t\t<option selected=\"selected\">0";
                    echo twig_escape_filter($this->env, (isset($context["day"]) ? $context["day"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 33
                    echo "\t\t\t\t\t<option>0";
                    echo twig_escape_filter($this->env, (isset($context["day"]) ? $context["day"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 35
                echo "\t\t\t\t\t";
            } else {
                // line 36
                echo "\t\t\t\t\t\t";
                if (((isset($context["day"]) ? $context["day"] : null) == twig_date_format_filter($this->env, "now", "d"))) {
                    // line 37
                    echo "\t\t\t\t\t<option selected=\"selected\">";
                    echo twig_escape_filter($this->env, (isset($context["day"]) ? $context["day"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 39
                    echo "\t\t\t\t\t<option>";
                    echo twig_escape_filter($this->env, (isset($context["day"]) ? $context["day"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 41
                echo "\t\t\t\t\t";
            }
            // line 42
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['day'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "\t\t\t\t</select>
\t\t\t\t-
\t\t\t\t<select name=\"date_month\">
\t\t\t\t";
        // line 46
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["month"]) {
            // line 47
            echo "\t\t\t\t\t";
            if (((isset($context["month"]) ? $context["month"] : null) < 10)) {
                // line 48
                echo "\t\t\t\t\t\t";
                if (((isset($context["month"]) ? $context["month"] : null) == twig_date_format_filter($this->env, "now", "m"))) {
                    // line 49
                    echo "\t\t\t\t\t<option selected=\"selected\">0";
                    echo twig_escape_filter($this->env, (isset($context["month"]) ? $context["month"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 51
                    echo "\t\t\t\t\t<option>0";
                    echo twig_escape_filter($this->env, (isset($context["month"]) ? $context["month"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 53
                echo "\t\t\t\t\t";
            } else {
                // line 54
                echo "\t\t\t\t\t\t";
                if (((isset($context["month"]) ? $context["month"] : null) == twig_date_format_filter($this->env, "now", "m"))) {
                    // line 55
                    echo "\t\t\t\t\t<option selected=\"selected\">";
                    echo twig_escape_filter($this->env, (isset($context["month"]) ? $context["month"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 57
                    echo "\t\t\t\t\t<option>";
                    echo twig_escape_filter($this->env, (isset($context["month"]) ? $context["month"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 59
                echo "\t\t\t\t\t";
            }
            // line 60
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['month'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "\t\t\t\t</select>
\t\t\t\t-
\t\t\t\t<select name=\"date_year\">
\t\t\t\t";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1960, 2014));
        foreach ($context['_seq'] as $context["_key"] => $context["year"]) {
            // line 65
            echo "\t\t\t\t\t";
            if (((isset($context["year"]) ? $context["year"] : null) == twig_date_format_filter($this->env, "now", "Y"))) {
                // line 66
                echo "\t\t\t\t\t<option selected=\"selected\">";
                echo twig_escape_filter($this->env, (isset($context["year"]) ? $context["year"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t";
            } else {
                // line 68
                echo "\t\t\t\t\t<option>";
                echo twig_escape_filter($this->env, (isset($context["year"]) ? $context["year"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t";
            }
            // line 70
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['year'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "\t\t\t\t</select>
\t\t\t\t
\t\t\t\t<select name=\"date_hour\">
\t\t\t\t";
        // line 74
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 23));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 75
            echo "\t\t\t\t\t";
            if (((isset($context["hour"]) ? $context["hour"] : null) < 10)) {
                // line 76
                echo "\t\t\t\t\t\t";
                if (((isset($context["hour"]) ? $context["hour"] : null) == twig_date_format_filter($this->env, "now", "H"))) {
                    // line 77
                    echo "\t\t\t\t\t<option selected=\"selected\">0";
                    echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 79
                    echo "\t\t\t\t\t<option>0";
                    echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 81
                echo "\t\t\t\t\t";
            } else {
                // line 82
                echo "\t\t\t\t\t\t";
                if (((isset($context["hour"]) ? $context["hour"] : null) == twig_date_format_filter($this->env, "now", "H"))) {
                    // line 83
                    echo "\t\t\t\t\t<option selected=\"selected\">";
                    echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 85
                    echo "\t\t\t\t\t<option>";
                    echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 87
                echo "\t\t\t\t\t";
            }
            // line 88
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "\t\t\t\t</select>
\t\t\t\t:
\t\t\t\t<select name=\"date_minute\">
\t\t\t\t";
        // line 92
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 59));
        foreach ($context['_seq'] as $context["_key"] => $context["minute"]) {
            // line 93
            echo "\t\t\t\t\t";
            if (((isset($context["minute"]) ? $context["minute"] : null) < 10)) {
                // line 94
                echo "\t\t\t\t\t\t";
                if (((isset($context["hour"]) ? $context["hour"] : null) == twig_date_format_filter($this->env, "now", "i"))) {
                    // line 95
                    echo "\t\t\t\t\t<option selected=\"selected\">0";
                    echo twig_escape_filter($this->env, (isset($context["minute"]) ? $context["minute"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 97
                    echo "\t\t\t\t\t<option>0";
                    echo twig_escape_filter($this->env, (isset($context["minute"]) ? $context["minute"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 99
                echo "\t\t\t\t\t";
            } else {
                // line 100
                echo "\t\t\t\t\t\t";
                if (((isset($context["minute"]) ? $context["minute"] : null) == twig_date_format_filter($this->env, "now", "i"))) {
                    // line 101
                    echo "\t\t\t\t\t<option selected=\"selected\">";
                    echo twig_escape_filter($this->env, (isset($context["minute"]) ? $context["minute"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                } else {
                    // line 103
                    echo "\t\t\t\t\t<option>";
                    echo twig_escape_filter($this->env, (isset($context["minute"]) ? $context["minute"] : null), "html", null, true);
                    echo "</option>
\t\t\t\t\t\t";
                }
                // line 105
                echo "\t\t\t\t\t";
            }
            // line 106
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['minute'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "\t\t\t\t</select>
\t\t\t</li>
\t\t\t
\t\t\t<li>
\t\t\t\t<h2>Substances</h2>
\t\t\t\t
\t\t\t\t<ul id=\"substances\">
\t\t\t\t\t<li id=\"substance_entry\">
\t\t\t\t\t\tT+
\t\t\t\t\t\t<select name=\"dose_hour[0]\">
\t\t\t\t\t\t";
        // line 117
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 48));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 118
            echo "\t\t\t\t\t\t\t";
            if (((isset($context["hour"]) ? $context["hour"] : null) < 10)) {
                // line 119
                echo "\t\t\t\t\t\t\t<option>0";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            } else {
                // line 121
                echo "\t\t\t\t\t\t\t<option>";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            }
            // line 123
            echo "\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t\t:
\t\t\t\t\t\t<select name=\"dose_minute[0]\">
\t\t\t\t\t\t";
        // line 127
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 59));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 128
            echo "\t\t\t\t\t\t\t";
            if (((isset($context["hour"]) ? $context["hour"] : null) < 10)) {
                // line 129
                echo "\t\t\t\t\t\t\t<option>0";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            } else {
                // line 131
                echo "\t\t\t\t\t\t\t<option>";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            }
            // line 133
            echo "\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t
\t\t\t\t\t\t<select name=\"substance[0]\">
\t\t\t\t\t\t\t<option value=\"0\">None</option>
\t\t\t\t\t\t";
        // line 138
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["substances"]) ? $context["substances"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["substance"]) {
            // line 139
            echo "\t\t\t\t\t\t\t";
            if (twig_in_filter("- ", (isset($context["substance"]) ? $context["substance"] : null))) {
                // line 140
                echo "\t\t\t\t\t\t\t<option disabled>";
                echo twig_escape_filter($this->env, (isset($context["substance"]) ? $context["substance"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            } else {
                // line 142
                echo "\t\t\t\t\t\t\t<option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["substance"]) ? $context["substance"] : null), 1), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["substance"]) ? $context["substance"] : null), 0), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t";
            }
            // line 144
            echo "\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['substance'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t<select name=\"dose[0]\">
\t\t\t\t\t\t";
        // line 148
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 9999));
        foreach ($context['_seq'] as $context["_key"] => $context["dose"]) {
            // line 149
            echo "\t\t\t\t\t\t\t<option>";
            echo twig_escape_filter($this->env, (isset($context["dose"]) ? $context["dose"] : null), "html", null, true);
            echo "</option>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dose'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t<select name=\"dose_unit[0]\">
\t\t\t\t\t\t\t<option value=\"2\">mg</option>
\t\t\t\t\t\t\t<option value=\"1\">&micro;g</option>
\t\t\t\t\t\t\t<option value=\"3\">g</option>
\t\t\t\t\t\t\t<option value=\"7\">mL</option>
\t\t\t\t\t\t\t<option value=\"6\">&micro;L</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t\t
\t\t\t\t\t\t<button type=\"button\" class=\"list_add\" id=\"substance_add\">+</button>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t\t
\t\t\t\t<ol class=\"notes\">
\t\t\t\t\t<li>To enter a value, rather than to scroll, you can simply highlight the input box and type.</li>
\t\t\t\t\t<li>You can't enter fractions. Therefore, for example, enter 1500mg if you took 1.5g.</li>
\t\t\t\t\t<li>Can't find the substance you're looking for? Check the aliases table, or contact us!</li>
\t\t\t\t</ol>
\t\t\t</li>
\t\t\t
\t\t\t<li>
\t\t\t\t<h2>Timeline</h2>
\t\t\t\t
\t\t\t\t<ul id=\"timeline\" class=\"timeline\">
\t\t\t\t\t<li id=\"timeline_entry\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\tT+
\t\t\t\t\t\t\t\t<select name=\"timeline_hour[0]\">
\t\t\t\t\t\t\t\t";
        // line 181
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 48));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 182
            echo "\t\t\t\t\t\t\t\t\t";
            if (((isset($context["hour"]) ? $context["hour"] : null) < 10)) {
                // line 183
                echo "\t\t\t\t\t\t\t\t\t<option>0";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 185
                echo "\t\t\t\t\t\t\t\t\t<option>";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 187
            echo "\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 188
        echo "\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t:
\t\t\t\t\t\t\t\t<select name=\"timeline_minute[0]\">
\t\t\t\t\t\t\t\t";
        // line 191
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 59));
        foreach ($context['_seq'] as $context["_key"] => $context["hour"]) {
            // line 192
            echo "\t\t\t\t\t\t\t\t\t";
            if (((isset($context["hour"]) ? $context["hour"] : null) < 10)) {
                // line 193
                echo "\t\t\t\t\t\t\t\t\t<option>0";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t";
            } else {
                // line 195
                echo "\t\t\t\t\t\t\t\t\t<option>";
                echo twig_escape_filter($this->env, (isset($context["hour"]) ? $context["hour"] : null), "html", null, true);
                echo "</option>
\t\t\t\t\t\t\t\t\t";
            }
            // line 197
            echo "\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hour'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 198
        echo "\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<select name=\"timeline_introspection[0]\">
\t\t\t\t\t\t\t\t\t<option value=\"unselected\" selected=\"selected\" disabled>Introspection</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">0</option>
\t\t\t\t\t\t\t\t\t<option value=\"-4\">- - - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-3\">- - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-2\">- -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-1\">-</option>
\t\t\t\t\t\t\t\t\t<option value=\"?\">+/-</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">+</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">+ +</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">+ + +</option>
\t\t\t\t\t\t\t\t\t<option value=\"4\">+ + + +</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<select name=\"timeline_visuals[0]\">
\t\t\t\t\t\t\t\t\t<option value=\"unselected\" selected=\"selected\" disabled>Visuals</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">0</option>
\t\t\t\t\t\t\t\t\t<option value=\"-4\">- - - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-3\">- - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-2\">- -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-1\">-</option>
\t\t\t\t\t\t\t\t\t<option value=\"?\">+/-</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">+</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">+ +</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">+ + +</option>
\t\t\t\t\t\t\t\t\t<option value=\"4\">+ + + +</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<select name=\"timeline_bodyload[0]\">
\t\t\t\t\t\t\t\t\t<option value=\"unselected\" selected=\"selected\" disabled>Bodyload</option>
\t\t\t\t\t\t\t\t\t<option value=\"0\">0</option>
\t\t\t\t\t\t\t\t\t<option value=\"-4\">- - - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-3\">- - -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-2\">- -</option>
\t\t\t\t\t\t\t\t\t<option value=\"-1\">-</option>
\t\t\t\t\t\t\t\t\t<option value=\"?\">+/-</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\">+</option>
\t\t\t\t\t\t\t\t\t<option value=\"2\">+ +</option>
\t\t\t\t\t\t\t\t\t<option value=\"3\">+ + +</option>
\t\t\t\t\t\t\t\t\t<option value=\"4\">+ + + +</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<textarea name=\"timeline_note[0]\" placeholder=\"Notes\" class=\"note\"></textarea>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"list_add\" id=\"timeline_add\">+</button>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t\t\t
\t\t\t\t<button type=\"button\" id=\"ratescale_toggle\" class=\"toggle\">Show rating scale and summary</button>\t
\t\t\t\t<table class=\"notes\" id=\"ratescale\" style=\"display: none;\">
\t\t\t\t\t<tr><td style=\"width: 60px;\">- - - -</td><td><b>Traumatic</b>: The experience has become insuperably lunatic.</td></tr>
\t\t\t\t\t<tr><td>- - -</td><td><b>Overwhelming</b>: Negative effects are intruiging and unavoidable.</td></tr>
\t\t\t\t\t<tr><td>- -</td><td><b>Unsettling</b>: The effects have a disturbing influence.</td></tr>
\t\t\t\t\t<tr><td>-</td><td><b>Concerning</b>: A rather unpleasant atmosphere is present.</td></tr>
\t\t\t\t\t<tr><td>0</td><td><b>None</b>: There is no observable effect whatsoever.</td></tr>
\t\t\t\t\t<tr><td>+/-</td><td><b>Uncertain</b>: Perceived effects may or may not be placebo.</td></tr>
\t\t\t\t\t<tr><td>+</td><td><b>Noticeable</b>: There are definite signs of desirable activity.</td></tr>
\t\t\t\t\t<tr><td>+ +</td><td><b>Significant</b>: The effects undeniably enhance the experience.</td></tr>
\t\t\t\t\t<tr><td>+ + +</td><td><b>Indulging</b>: The experience is lead by insuppressible bliss.</td></tr>
\t\t\t\t\t<tr><td>+ + + +</td><td><b>Transcendental</b>: There is no lucidity to what is happening.</td></tr>
\t\t\t\t</table>
\t\t\t</li>
\t\t\t
\t\t\t<li>
\t\t\t\t<h2>Global</h2>
\t\t\t
\t\t\t\t<textarea placeholder=\"Vent yourself!\" name=\"notes\"></textarea>
\t\t\t</li>
\t\t\t
\t\t\t<li><input type=\"submit\" value=\"Add\" name=\"add\"></li>
\t\t</ul>
\t</form>
</form>
\t\t\t
<script type=\"text/javascript\">
(function(document){
\tvar substancelist = document.querySelector('#substances');
\tdocument.querySelector(\"#substance_add\").onclick = function() {
\t\tvar entry = substancelist.querySelector('#substance_entry');
\t\tvar entry_new = entry.cloneNode(true);
\t\tvar subcount = substancelist.querySelectorAll('li').length;

\t\tentry_new.id = 'substance_entry['+subcount+']';
\t\tvar selectSet = entry_new.querySelectorAll(\"SELECT\");
\t\tvar selectSetOrig = entry.querySelectorAll(\"SELECT\");

\t\tfor(var i = 0; i < selectSet.length; i++){
\t\t\tvar currentName = selectSet[i].name;
\t\t\tvar nameRegex = /(.*)\\[\\d+\\]/g;
\t\t\tvar matches = nameRegex.exec(currentName);
\t\t\tselectSet[i].name = matches[1] + \"[\" +  subcount + \"]\";\t
\t\t\tselectSet[i].selectedIndex = selectSetOrig[i].selectedIndex;
\t\t\tselectSetOrig[i].selectedIndex = 0;
\t\t\tselectSetOrig[i].name = matches[1] + \"[0]\";\t
\t\t}

\t\tentry_new.removeChild(entry_new.querySelector('#substance_add'));
\t\tsubstancelist.insertBefore(entry_new, entry);
\t}
})(document);

(function(document){
\tvar timeline = document.querySelector(\"#timeline\");
\tdocument.querySelector(\"#timeline_add\").onclick = function(){
\t\tvar t_event = timeline.querySelector(\"#timeline_entry\");
\t\tvar t_new = t_event.cloneNode(true);
\t\tvar t_count = timeline.querySelectorAll(\"#timeline_entry\").length;

\t\tt_new.id = 'timeline_entry['+t_count+']';
\t\tvar selectSet = t_new.querySelectorAll(\"SELECT\");
\t\tvar selectSetOrig = t_event.querySelectorAll(\"SELECT\");\t\t

\t\tfor(var i = 0; i < selectSet.length; i++){
\t\t\tvar currentName = selectSet[i].name;
\t\t\tvar nameRegex = /(.*)\\[\\d+\\]/g;
\t\t\tvar matches = nameRegex.exec(currentName);
\t\t\tselectSet[i].name = matches[1] + \"[\" +  t_count + \"]\";\t
\t\t\tselectSet[i].selectedIndex = selectSetOrig[i].selectedIndex;
\t\t\tselectSetOrig[i].selectedIndex = 0;
\t\t\tselectSetOrig[i].name = matches[1] + \"[0]\";\t
\t\t}

\t\tvar inputSet = t_new.querySelectorAll(\"INPUT\");
\t\tvar inputSetOrig = t_event.querySelectorAll(\"INPUT\");

\t\tfor (var i = 0; i < inputSet.length; i++){
\t\t\tvar currentName = inputSet[i].name;
\t\t\tvar nameRegex = /(.*)\\[\\d+\\]/g;
\t\t\tvar matches = nameRegex.exec(currentName);
\t\t\tinputSet[i].name = matches[1] + \"[\" +  t_count + \"]\";\t
\t\t\tinputSet[i].value = inputSetOrig[i].value;
\t\t\tinputSetOrig[i].value = \"\";
\t\t\tinputSetOrig[i].name = matches[1] + \"[0]\";\t\t\t\t
\t\t}

\t\t//t_new.removeChild(t_new.querySelector('LI > #timeline_add'));
\t\tvar buttonEl = t_new.querySelector(\"#timeline_add\");
\t\tbuttonEl.parentNode.removeChild(buttonEl);
\t\ttimeline.insertBefore(t_new, t_event); 
\t}
})(document);

\$(\"#ratescale_toggle\").click(function() {
\t\$(\"#ratescale\").toggle(1, function() {
\t\tif(\$(\"#ratescale_toggle\").text() == \"Show rating scale and summary\") {
\t\t\t\$(\"#ratescale_toggle\").text(\"Hide rating scale and summary\");
\t\t} else {
\t\t\t\$(\"#ratescale_toggle\").text(\"Show rating scale and summary\");
\t\t}
\t});
});
</script>
";
    }

    public function getTemplateName()
    {
        return "views/exp/add.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  520 => 198,  514 => 197,  508 => 195,  502 => 193,  499 => 192,  495 => 191,  490 => 188,  484 => 187,  478 => 185,  472 => 183,  469 => 182,  465 => 181,  433 => 151,  424 => 149,  420 => 148,  415 => 145,  409 => 144,  401 => 142,  395 => 140,  392 => 139,  388 => 138,  382 => 134,  376 => 133,  370 => 131,  364 => 129,  361 => 128,  357 => 127,  352 => 124,  346 => 123,  340 => 121,  334 => 119,  331 => 118,  327 => 117,  315 => 107,  309 => 106,  306 => 105,  300 => 103,  294 => 101,  291 => 100,  288 => 99,  282 => 97,  276 => 95,  273 => 94,  270 => 93,  266 => 92,  261 => 89,  255 => 88,  252 => 87,  246 => 85,  240 => 83,  237 => 82,  234 => 81,  228 => 79,  222 => 77,  219 => 76,  216 => 75,  212 => 74,  207 => 71,  201 => 70,  195 => 68,  189 => 66,  186 => 65,  182 => 64,  177 => 61,  171 => 60,  168 => 59,  162 => 57,  156 => 55,  153 => 54,  150 => 53,  144 => 51,  138 => 49,  135 => 48,  132 => 47,  128 => 46,  123 => 43,  117 => 42,  114 => 41,  108 => 39,  102 => 37,  99 => 36,  96 => 35,  90 => 33,  84 => 31,  81 => 30,  78 => 29,  74 => 28,  60 => 16,  56 => 14,  54 => 13,  51 => 12,  45 => 10,  43 => 9,  38 => 6,  35 => 5,  29 => 3,);
    }
}
