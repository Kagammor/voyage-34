<?php

/* views/auth/register.html */
class __TwigTemplate_43828631109ce98349cfd135bd0b8344e35a28f5d559dd451a2d1e74b9154a28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Voyage 34: Register";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<section class=\"auth_register\">
\t<h1>Register</h1>
\t
\t";
        // line 9
        if (array_key_exists("error", $context)) {
            // line 10
            echo "\t\t<span class=\"error\">";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "</span>
\t";
        }
        // line 12
        echo "\t
\t";
        // line 13
        if (array_key_exists("success", $context)) {
            // line 14
            echo "\t\t<span class=\"success\">Registration complete!</span>
\t";
        }
        // line 16
        echo "\t
\t<form method=\"post\" action=\"./proc/auth.php\">
\t\t<ul>
\t\t\t<li><input type=\"text\" maxlength=\"20\" placeholder=\"username\" name=\"username\"></li>
\t\t\t<li><input type=\"password\" placeholder=\"password\" name=\"password_one\"></li>
\t\t\t<li><input type=\"password\" placeholder=\"password (repeat)\" name=\"password_two\"></li>
\t\t\t<li style=\"position: relative;\"><input type=\"text\" placeholder=\"e-mail\" name=\"email\"></li>
\t\t
\t\t\t<li><input type=\"submit\" value=\"Register\" name=\"register\"></li>
\t\t\t
\t\t\t<p class=\"notes\">
\t\t\t\tUncomfortable with sharing your e-mail address?<br>
\t\t\t\tFeel free to use an e-mail address that's not associated with your daily business.
\t\t\t</p>
\t\t</ul>
\t</form>
</form>
";
    }

    public function getTemplateName()
    {
        return "views/auth/register.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  56 => 14,  54 => 13,  51 => 12,  45 => 10,  43 => 9,  38 => 6,  35 => 5,  29 => 3,);
    }
}
