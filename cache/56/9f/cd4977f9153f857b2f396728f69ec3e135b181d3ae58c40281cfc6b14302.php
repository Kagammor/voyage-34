<?php

/* views/profile.html */
class __TwigTemplate_569fcd4977f9153f857b2f396728f69ec3e135b181d3ae58c40281cfc6b14302 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("base.html");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Voyage 34: Welcome";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "<h1>";
        echo twig_escape_filter($this->env, (isset($context["username"]) ? $context["username"] : null), "html", null, true);
        echo "</h1>

<h2>Experiences</h2>
";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["experiences"]) ? $context["experiences"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
            // line 10
            echo "<div class=\"experience\">
\t";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["experience"]) ? $context["experience"] : null), "html", null, true);
            echo "
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "
<a href=\"./exp?action=add\"><button type=\"button\" class=\"button_addxp\">Add experience</button></a>
";
    }

    public function getTemplateName()
    {
        return "views/profile.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 14,  52 => 11,  49 => 10,  45 => 9,  38 => 6,  35 => 5,  29 => 3,);
    }
}
