<?php
	session_start();
	
	require_once("./proc/config.php");
	require_once("./vendor/autoload.php");

	$loader = new Twig_Loader_Filesystem('./templates');
	$twig = new Twig_Environment($loader, array(
		'cache' => './cache',
		'auto_reload' => TRUE
	));

	$template = $twig->loadTemplate('views/profile.html');
	
	// Connect to the database
	$mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
	
	if($mysqli->connect_errno) {
		echo "Failed to connect to database: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	
	$query = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_SESSION['auth']) . "'";
	$result = $mysqli->query($query);

	if($row = $result->fetch_assoc()) {
		$user = $row['id'];
	}
	
	$experiences = array();
	
	$query = "SELECT * FROM experiences WHERE user='" . $user . "'";
	$result = $mysqli->query($query);
		
	while($row = $result->fetch_assoc()) {
		array_push($experiences, $row['id']);
	}
	
	if(isset($_SESSION['auth'])) {
		$template->display(array('username' => $_SESSION['auth'], 'experiences' => $experiences));
	} else {
		header('Location: /auth?action=login&fb=211');
	}
?>
