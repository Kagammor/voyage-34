<?php
	session_start();

	require_once("./proc/config.php");
	require_once("./proc/errors.php");
	require_once("./vendor/autoload.php");

	$loader = new Twig_Loader_Filesystem('./templates');
	$twig = new Twig_Environment($loader, array(
		'cache' => './cache',
		'auto_reload' => TRUE
	));

	$add = $twig->loadTemplate('views/exp/add.html');
	
	// Connect to the database
	$mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);

	// Check if MySQL connection can be made
	if (mysqli_connect_errno())
	{
		echo "The connection to the MySQL database failed:<br />" . mysqli_connect_error();
		exit();
	}
	
	/* CREATE SUBSTANCE LIST */
	$list = array();
	$ids = array();
	
	/* Stimulants */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '1%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$stimulants = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($stimulants, $row_sub);
	}
	
	array_push($list, '- Stimulants');
	
	foreach($stimulants as $stimulant) {
		if(strlen($stimulant['aliases']) > 1) {
			$alias = explode("|", $stimulant['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($stimulant['name'] . $alias, $stimulant['id']);
		
		array_push($list, $name);
	}
	
	/* Depressants */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '2%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$depressants = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($depressants, $row_sub);
	}
	
	array_push($list, '- Depressants');
	
	foreach($depressants as $depressant) {
		if(strlen($depressant['aliases']) > 1) {
			$alias = explode("|", $depressant['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($depressant['name'] . $alias, $depressant['id']);
		
		array_push($list, $name);
	}
	
	/* Psychedelics */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '3%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$psychedelics = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($psychedelics, $row_sub);
	}
	
	array_push($list, '- Psychedelics');
	
	foreach($psychedelics as $psychedelic) {
		if(strlen($psychedelic['aliases']) > 1) {
			$alias = explode("|", $psychedelic['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($psychedelic['name'] . $alias, $psychedelic['id']);
		
		array_push($list, $name);
	}
	
	/* Opioids */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '4%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$opioids = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($opioids, $row_sub);
	}
	
	array_push($list, '- Opioids');
	
	foreach($opioids as $opioid) {
		if(strlen($opioid['aliases']) > 1) {
			$alias = explode("|", $opioid['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($opioid['name'] . $alias, $opioid['id']);
		
		array_push($list, $name);
	}
	
	/* Dissociatives */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '5%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$dissociatives = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($dissociatives, $row_sub);
	}
	
	array_push($list, '- Dissociatives');
	
	foreach($dissociatives as $dissociative) {
		if(strlen($dissociative['aliases']) > 1) {
			$alias = explode("|", $dissociative['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($dissociative['name'] . $alias, $dissociative['id']);
		
		array_push($list, $name);
	}
	
	/* Cannabinoids */
	$query_sub = "SELECT * FROM substances WHERE id LIKE '6%%%' ORDER BY collect,name";
	$result_sub = $mysqli->query($query_sub);
	
	$cannabinoids = array();
	
	while($row_sub = $result_sub->fetch_assoc()) {
		array_push($cannabinoids, $row_sub);
	}
	
	array_push($list, '- Cannabinoids');
	
	foreach($cannabinoids as $cannabinoid) {
		if(strlen($cannabinoid['aliases']) > 1) {
			$alias = explode("|", $cannabinoid['aliases']);
			$alias = ' (' . ucfirst($alias[0]) . ')';
		} else {
			$alias = "";
		};
		
		$name = array($cannabinoid['name'] . $alias, $cannabinoid['id']);
		
		array_push($list, $name);
	}
	
	if(isset($_SESSION['auth'])) {
		if($_GET['action'] == 'add') {
			if(isset($_GET['fb'])) {
				$add->display(array('substances' => $list, 'username' => $_SESSION['auth'], 'errorid' => $_GET['fb'], 'error' => getError($_GET['fb'])));
			} else {
				$add->display(array('substances' => $list, 'username' => $_SESSION['auth']));
			}
		} else {
			header('Location: /');
		}
	} else {
		header('Location: /auth?action=login&fb=211');
	}
?>
