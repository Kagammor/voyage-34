<?php
	session_start();
	
	require_once("./config.php");
	
	// Connect to the database
	$mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
	
	if($mysqli->connect_errno) {
		echo "Failed to connect to database: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	if(isset($_POST['add'])) {
		$nosubstance = true;
	
		foreach($_POST['substance'] as $substance) {
			if($substance == 0 && $nosubstance != false) {
				$nosubstance = true;
			} else {
				$nosubstance = false;
			}
		}
		
		if(!$nosubstance) {
			$query = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_SESSION['auth']) . "'";
			$result = $mysqli->query($query);
		
			if($row = $result->fetch_assoc()) {
				$user = $row['id'];
			}
	
			$experience = md5($_SESSION['auth'] . uniqid());
			$date = $_POST['date_year'] . '-' . $_POST['date_month'] . '-' . $_POST['date_day'] . ' ' . $_POST['date_hour'] . ':' . $_POST['date_minute'] . ':00';
		
			$query = "INSERT INTO experiences (id, user, date, name, notes) 
			VALUES ('" . $experience . "', '"
			 . mysqli_real_escape_string($mysqli, $user) . "', '" 
			 . mysqli_real_escape_string($mysqli, $date) . "', '" 
			 . mysqli_real_escape_string($mysqli, $_POST['title']) . "', '" 
			 . mysqli_real_escape_string($mysqli, $_POST['notes']) . "')";
				 
			$result = $mysqli->query($query);
			 
			if(!$result) {
				echo "Adding experience failed.<br>";
				exit();
			} else {
				echo "Adding experience completed.<br>";
			}

			$substances = array_values($_POST['substance']);
			$dose_hours = array_values($_POST['dose_hour']);
			$dose_minutes = array_values($_POST['dose_minute']);
			$dose = array_values($_POST['dose']);
			$dose_unit = array_values($_POST['dose_unit']);
		
			foreach($substances as $i => $substance) {
				$time = $dose_hours[$i] . ':' . $dose_minutes[$i];
			
				$query = "INSERT INTO timeline_subs (experience, time, substance, dose, unit) 
				VALUES ('" . $experience . "', '"
				 . mysqli_real_escape_string($mysqli, $time) . "', '" 
				 . mysqli_real_escape_string($mysqli, $substance) . "', '"
				 . mysqli_real_escape_string($mysqli, $dose[$i]) . "', '"
				 . mysqli_real_escape_string($mysqli, $dose_unit[$i]) . "')";
				 
				$result = $mysqli->query($query);
				 
				if(!$result) {
					echo "Adding substance timeline failed.<br>";
					exit();
				} else {
					echo "Adding substance timeline completed.<br>";
				}
			}
			
			$notes = array_values($_POST['timeline_note']);
			$timeline_hours = array_values($_POST['timeline_hour']);
			$timeline_minutes = array_values($_POST['timeline_minute']);
			$introspection = array_values($_POST['timeline_introspection']);
			$visuals = array_values($_POST['timeline_visuals']);
			$bodyload = array_values($_POST['timeline_bodyload']);
			
			foreach($_POST['timeline_note'] as $i => $note) {
				$time = $timeline_hours[$i] . ':' . $timeline_minutes[$i];
			
				$query = "INSERT INTO timeline_notes (experience, time, introspection, visuals, bodyload, note) 
				VALUES ('" . $experience . "', '"
				 . mysqli_real_escape_string($mysqli, $time) . "', '" 
				 . mysqli_real_escape_string($mysqli, $introspection[$i]) . "', '"
				 . mysqli_real_escape_string($mysqli, $visuals[$i]) . "', '"
				 . mysqli_real_escape_string($mysqli, $bodyload[$i]) . "', '"
				 . mysqli_real_escape_string($mysqli, $notes[$i]) . "')";
				 
				$result = $mysqli->query($query);
				 
				if(!$result) {
					echo "Adding note timeline failed.<br>";
					exit();
				} else {
					header('Location: /profile');
				}
			}
		} else {
			header('Location: /exp?action=add&fb=401');
		}
	}
?>
