<?php
	session_start();

	require_once("./config.php");
	
	// Connect to the database
	$mysqli = new mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
	
	if($mysqli->connect_errno) {
		echo "Failed to connect to database: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	
			
	$query_user = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_POST['username']) . "'";
	$result_user = $mysqli->query($query_user);
	
	/*
	------------------
		  LOGIN
	------------------
	*/
	if(isset($_POST['login'])) {
		$query_user = "SELECT * FROM users WHERE username='" . mysqli_real_escape_string($mysqli, $_POST['username']) . "'";
		$result_user = $mysqli->query($query_user);
		$row_user = $result_user->fetch_assoc();
		
		$hash = hash('sha256', hash('md5', $_POST['password']));
		
		if($result_user->num_rows < 1) {
			header('Location: ../auth/login?fb=201');
		} elseif(!password_verify($_POST['password'], $row_user['password'])) {
			header('Location: ../auth/login?fb=202');
		} elseif($row_user['approved'] != 1) {
			header('Location: ../auth/login?fb=209');
		} else {
			$_SESSION['auth'] = $row_user['username'];
			header('Location: /');
		}
	/*
	------------------
		 REGISTER
	------------------
	*/
	} elseif(isset($_POST['register'])) {
		if(strlen($_POST['username']) < 2) {
			header('Location: ../auth/register?fb=206');
		} elseif ($result_user->num_rows > 0) {
			header('Location: ../auth/register?fb=208');
		} elseif(strlen($_POST['password_one']) < 2) {
			header('Location: ../auth/register?fb=207');
		} elseif($_POST['password_one'] != $_POST['password_two']) {
			header('Location: ../auth/register?fb=204');
		} elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			header('Location: ../auth/register?fb=205');
		} else {
			$query_auth = "INSERT INTO users (username, password, email) VALUES ('" . mysqli_real_escape_string($mysqli, $_POST['username']) . "', '" . mysqli_real_escape_string($mysqli, password_hash($_POST['password_one'], PASSWORD_DEFAULT)) . "', '" . mysqli_real_escape_string($mysqli, $_POST['email']) . "')";
			$result_auth = $mysqli->query($query_auth);
			
			if(!$result_auth) {
				header('Location: ../auth/register?fb=301');
				exit();
			} else {
				header('Location: ../auth/register?success');
			}
		}
	/*
	------------------
		  LOGOUT
	------------------
	*/
	} elseif(isset($_GET['logout'])) {
		unset($_SESSION['auth']);
		session_destroy();
		
		header('Location: /auth?action=login');
	/*
	------------------
		UNDEFINED
	------------------
	*/
	} else {
		header('Location: /');
	}
?>
