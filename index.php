<?php
	session_start();

	require_once("./vendor/autoload.php");

	$loader = new Twig_Loader_Filesystem('./templates');
	$twig = new Twig_Environment($loader, array(
		'cache' => './cache',
		'auto_reload' => TRUE
	));

	$template = $twig->loadTemplate('views/front.html');
	
	if(isset($_SESSION['auth'])) {
		$template->display(array('username' => $_SESSION['auth']));
	} else {
		$template->display(array());
	}
?>
